<?php
/**
 * scandir($path)    遍历一个文件夹所有文件并返回数组
 * unlink($filename)    删除文件
 * rmdir($path)    只删除空文件夹
 */

//设置需要删除的文件夹
$path = "./Application/Runtime/";
//清空文件夹函数和清空文件夹后删除空文件夹函数的处理
function deldir($path) {
    //如果是目录则继续
    if (is_dir($path)) {
        //传入路径字符串末尾加上"/"
        if (substr($path, -1) != "/") {
            $path .= "/";
        }
        //扫描一个文件夹内的所有文件夹和文件并返回数组
        $p = scandir($path);
        foreach ($p as $val) {
            //排除目录中的.和..
            if ($val != "." && $val != "..") {
                $_path = $path.$val;
                //如果是目录则递归子目录，继续操作
                if (is_dir($_path)) {
                    //子目录中操作删除文件夹和文件
                    deldir($_path);
                    //目录清空后删除空文件夹
                    @rmdir($_path);
                } else {
                    //如果是文件直接删除
                    unlink($_path);
                }
            }
        }
    }
}

//调用函数，传入路径
deldir($path);