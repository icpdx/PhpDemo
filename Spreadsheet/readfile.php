<?php
/**
 * User: liuxian
 * Date: 2018/10/25
 * Time: 16:49
 */

require_once "../vendor/autoload.php";

use \PhpOffice\PhpSpreadsheet\IOFactory;
use \PhpOffice\PhpSpreadsheet\Cell\Coordinate;

$spreadsheet = IOFactory::load("./file.xlsx"); // 载入Excel表格

# 这里和上面代码的效果都一样,好像就是只读区别吧 不是特别清楚,官网上也给出了很多读写的写法,应该用会少消耗资源
# 官网地址 https://phpspreadsheet.readthedocs.io/en/develop/topics/reading-and-writing-to-file/
//$reader = IOFactory::createReader('Xlsx');
//$reader->setReadDataOnly(TRUE);
//$spreadsheet = $reader->load('./file.xlsx'); //载入excel表格

$worksheet = $spreadsheet->getActiveSheet();

$highestRow = $worksheet->getHighestRow(); // 总行数
$highestColumn = $worksheet->getHighestColumn(); // 总列数

# 把列的索引字母转为数字 从1开始 这里返回的是最大列的索引
# 我尝试了下不用这块代码用以前直接循环字母的方式,拿不到数据
# 测试了下超过26个字母也是没有问题的
$highestColumnIndex = Coordinate::columnIndexFromString($highestColumn);
//var_dump($highestColumnIndex);

$data = [];
for ($row = 2; $row <= $highestRow; ++$row) { // 从第二行开始
    $row_data = [];
    for ($column = 1; $column <= $highestColumnIndex; $column++) {
        $row_data[] = $worksheet->getCellByColumnAndRow($column, $row)->getValue();
    }
    $data[] = $row_data;
}
var_export($data);
