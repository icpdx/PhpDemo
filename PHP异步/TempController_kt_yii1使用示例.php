<?php
/**
 * 模板消息发送api
 * User: lewiss
 * Date: 2018/8/24
 * Time: 17:12
 */

class TempController extends Api_pubController {

    public function actionIndex() {

        $limit = 50;

        $get_last_id = !empty($_GET['last_id']) ? (int)$_GET['last_id'] : 0;

        $list = Yii::app()->db->createCommand()
            ->select("user.id,user.wxopenid,member.m_name")
            ->from("{{school_member}} member")
            ->join("{{user}} user", "member.user_id=user.id")
            ->where("class_id=:class_id and member.type=3 and user.id>:last_id",
                [":class_id" => (int)$_GET['class_id'], ':last_id' => $get_last_id])
            ->order("user.id")
            ->limit($limit)
            ->queryAll();

        $data = [
            "school_id" => $_GET['school_id'],
            "class_id" => $_GET['class_id'],
            "class_name" => $_GET['class_name'],
            "teacher" => $_GET['teacher'],
            "title" => $_GET['title'],
        ];

        $school_id = Fun_filter::wwy_md5_set($_GET["school_id"]);
        $last_id = 0;
        foreach ($list as $value) {
            $last_id = $value['id'];
            $data['openid'] = $value['wxopenid'];
            $data['m_name'] = $value['m_name'];
            $data['url'] = "http://ktkids.cn/kt_edu/index.php?r=wap/default/class_trends&school_id={$school_id}&trend_id={$_GET['trend_id']}&trend_class_id={$_GET['class_id']}";
            $this->fso("/kt_edu/index.php?r=temp/send", $data);
        }

        if ($last_id) {
            $data['last_id'] = $last_id;
            $data['trend_id'] = $_GET['trend_id'];
            $this->fso('/kt_edu/index.php?r=temp', $data);
        }
    }

    public function actionSend() {
        $m_name = Yii::app()->request->getParam('m_name');
        $class_name = Yii::app()->request->getParam('class_name');
        $teacher = Yii::app()->request->getParam('teacher');
        $title = Yii::app()->request->getParam('title');
        $openid = Yii::app()->request->getParam('openid');
        $school_id = Yii::app()->request->getParam('school_id');
        $url = Yii::app()->request->getParam('url');


        $template_id = "SsiA1yYA2q6yq7lJiREgravOFf0ySAeOKkLiXxd_bl4";
        $data = array(
            '1' => array('name' => 'first', 'value' => '您好，亲爱的' . $m_name . '家长', 'color' => '#173177'),
            '2' => array('name' => 'keyword1', 'value' => $class_name, 'color' => '#173177'),
            '3' => array('name' => 'keyword2', 'value' => $teacher, 'color' => '#173177'),
            '4' => array('name' => 'keyword3', 'value' => date("Y年m月d日 H:i"), 'color' => '#173177'),
            '5' => array('name' => 'keyword4', 'value' => $title, 'color' => '#173177'),
            '6' => array('name' => 'remark', 'value' => '点击查看通知详情', 'color' => '#173177'),
        );
        $result = Fun_temp_kt::template($school_id, $template_id, $data, $openid, $url);
        Yii::log("rsp----" . json_encode($result));
    }

    private function fso($uri, $data) {
        $param = http_build_query($data);
        $host = "kkkttt.cn";
        $fp = fsockopen($host, 80);
        $out = "GET {$uri}&{$param}   HTTP/1.1\r\n";
        $out .= "Host: " . $host . "\r\n";
        $out .= "Connection: Close\r\n\r\n";
        stream_set_blocking($fp, true);
        stream_set_timeout($fp, 1);
        fwrite($fp, $out);
        usleep(1000);
        fclose($fp);
    }

}