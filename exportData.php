<?php
// 导出Excel表
function exportData($filename,$title,$data){
    header("Content-type: application/vnd.ms-excel");
    header("Content-disposition: attachment; filename="  . $filename . ".xls");
    if (is_array($title)){
        foreach ($title as $key => $value){
            echo $value."\t";
        }
    }
    echo "\n";
    if (is_array($data)){
        foreach ($data as $key => $value){
            foreach ($value as $_key => $_value){
                echo $_value."\t";
            }
            echo "\n";
        }
    }
}

exportData("test",['uid','name'],[
    [1,'天府1'],
    [2,'天府2'],
    [3,'天府3'],
]);