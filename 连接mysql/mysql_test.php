<?php
$servername = "127.0.0.1";
$username = "root";
$password = "34fXX11@#m0j.e";
$database = "hgsl";


//面向过程
// 创建连接
$connect = mysqli_connect($servername, $username, $password, $database, 3306);

// 检测连接
if (!$connect) {
    die("连接失败: " . mysqli_connect_error() . PHP_EOL);
}
echo "连接成功" . PHP_EOL;

mysqli_query($connect, 'set names utf8');

// 查询 转为数组输出
$sql = "select * from table_name";
$result = mysqli_query($connect, $sql);
$arr = array();//定义空数组
while ($row = mysqli_fetch_array($result)) {
    array_push($arr, $row);
}
var_dump($arr);

// 修改
$sql = "update table_name set column_1=123 where id=3";
$result = mysqli_query($connect, $sql);// 貌似没有语法错误都是返回true
var_dump($result);

// 面向对象
$mysqli = new mysqli($servername, $username, $password, $database, '3306');
// 检测连接
if ($mysqli->connect_error) {
    die("连接失败: " . $mysqli->connect_error . PHP_EOL);
}

$sql = "select * from table_name";
$result = $mysqli->query($sql);
$arr = array();//定义空数组
while ($row = mysqli_fetch_array($result)) {
    array_push($arr, $row);
}
var_dump($arr);
