<?php

$str = [];
for ($i = 0; $i <= 90; $i++) {

    $y = ($i / 26);
    if ($y >= 1) {
        $y = intval($y);
        $a = chr($y + 64);
        $b = chr($i - $y * 26 + 65);
        $str[] = $a . $b;
    } else {
        $str[] = chr($i + 65);
    }
}
print_r($str);

// yield 生成器方式生成字母组合 yield在大数据量的时候可以节省内存的使用
// 详细请看https://segmentfault.com/a/1190000012334856
function literalCombination($count)
{
    for ($i = 0; $i <= $count; $i++) {

        $y = ($i / 26);
        if ($y >= 1) {
            $y = intval($y);
            $a = chr($y + 64);
            $b = chr($i - $y * 26 + 65);
            yield $a . $b;
        } else {
            yield  chr($i + 65);
        }
    }
}

foreach (literalCombination(90) as $item) {
    echo "{$item}\n";
}